Pour le level06 on a un executable et un fichier php. En regardant le fichier
php remarque plusieurs fonctions preg_replace, dont une avec une regex du type
//e, c'est a dire que le contenu de cette regex sera executé.

En regardant en details les regexs du fichier php on peut voir que le programme
effectue ses regexs sur le fichier passé en paramètre, et qu'il cherche un
patern du type "[x une-suite-de-caracteres]", puis retire tous les '.' et '@' de
la partie "une-suite-de-caracteres", puis remplace les '[]' de la string de base
par des '()'.

Il nous reste plus qu'à créer un fichier contenant une ligne ressemblant au
patern, et en remplacant "une-suite-de-caracteres" par du code php qui executera
getflag pour écrire son contenu dans un fichier.

Pour ce faire il faut mettre le code php dans une balise : "${}" et le code
systeme entre '``', ce qui donnera au final : [x ${`getflag > /tmp/flag06`}] ou
[x ${`getflag`}]

Il nous reste plus qu'à lancer le programme avec notre fichier, et récuperer le
contenu du fichier /tmp/flag06 !
