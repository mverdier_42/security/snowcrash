Un simple ls dans le home de ce niveau nous a montré le fichier "level02.pcap".
Nous avons copié ce fichier depuis la VM à notre machine avec la commande scp:
scp -P 4242 xxx.xxx.xxx.xxx:level02.pcap .
Ce qui nous a permis d'ouvrir le fichier pcap avec le site cloudshark.
Apres une analyse du stream, nous avons ces deux lignes qui nous intéressent:

Password:
ft_wandr...NDRel.L0L

Où les "." sont les caracteres ascii 7f en hexa, soit "del".
En se connectant a l'utilisateur falg02 on a rentré le mot de passe suivant:
ft_waNDReL0L
(ft_wandr ->(del, del, del) ft_wa -> ft_waNDRel ->(del) ft_waNDRe -> ft_waNDReL0L).
