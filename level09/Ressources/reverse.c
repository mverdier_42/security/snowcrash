#include <string.h>
#include <stdio.h>

int		main(int ac, char **av)
{
	int		len;

	if (ac != 2)
		return (0);

	len = strlen(av[1]);
	for (int i = 0; i < len; i++) {
		printf("%c", (av[1][i] - i));
	}
	printf("\n");

	return (0);
}
