Pour ce niveau nous avons un executable qui demande un paramètre. quand on lui
donne une string en paramètre, le programme nous renvoie la string modifiée de
la forme : abcdefg = acegikm

Le programme nous retourne donc une string en incrementant chaque caractère par
son index dans la string.

Nous avons également un fichier token, contenant un mot de passe illisible,
contenant des caractères qui depassent le table ASCII.

On s'est dit que le mot de passe contenu dans le fichier token est un mot de
passe ayant été crypté par le programme, on a donc ecrit un programme réalisant
l'inverse, c'est à dire prendre une string en paramètre et renvoyer cette string
en décrémentant chaque caractères par son index dans la string.

On a fait des essais en copiant le contenu du fichier token et en le donnant à
notre programme sans succès, il restait des caractères illisibles. On s'est dit
qu'avec un simple copié / collé on avait pas récupéré la bonne chaine, on a
donc appellé notre programme en lui donnant comme paramètre le résultat de la
commande "cat" avec le fichier token (./reverse `cat ~/token`).

Ce dernier essai nous on donné quelque chose de plus lisible, et on a pu se
connecter a l'utilisateur flag09 avec.
